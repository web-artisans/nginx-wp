# WordPress Pod Proxy for Kubernetes

This image exposes port 80 and contains boilerplate configuration to reverse proxy a WordPress installation.

It assumes that the following services are exposed in the pod:

## WordPress PHP-FPM Service [9000]

This configuration expects the service to be based on the [Web Artisans WordPress FPM](https://bitbucket.org/web-artisans/wp-fpm) Docker image.